## Import the needed libraries
import requests
import uuid
from decouple import config


# Env vars passed by the config object
CLIENT_ID = config('CLIENT_ID')
CLIENT_SECRET = config('CLIENT_SECRET')
API_URL = 'https://api.getport.io/v1'


## Try getting the Access Token from the Port API
credentials = {'clientId': CLIENT_ID, 'clientSecret': CLIENT_SECRET}
try:
    token_response = requests.post(f'{API_URL}/auth/access_token', json=credentials)
    token_response.raise_for_status() # This will raise an exception for HTTP status codes >= 400
    access_token = token_response.json()['accessToken']
    headers = {'Authorization': f'Bearer {access_token}'} # We can now use the value in access_token to make further requests


# Handle any HTTP error that may occur
except requests.exceptions.HTTPError as err:
    print(f'HTTP error occurred: {err}') 
    raise SystemExit(err) # Stop further execution in case of error

# Handle any other error that may occur
except Exception as err:
    print(f'Other error occurred: {err}')
    raise SystemExit(err) # Stop further execution in case of error


# We can now use the value in access_token to make further requests
headers = {
	'Authorization': f'Bearer {access_token}'
}

## Reference the blueprint
blueprint_id = 'gitlab_commit'

# Get entity variables
COMMIT_BY = config('COMMIT_BY')
COMMIT_HASH = config('COMMIT_HASH')
ACTION_JOB= config('ACTION_JOB')
PUSHED_AT = config('PUSHED_AT')
RUN_LINK = config('RUN_LINK')


## create a unique ID
identifier_id = uuid.uuid4().hex

## Prepare the entity object
entity = {
  "identifier": identifier_id,
  "title": "Stream Gitlab Commits",
  "properties": {
    "version": "0.01",
    "committedBy": COMMIT_BY,
    "commitHash": COMMIT_HASH,
    "actionJob": ACTION_JOB,
    "repoPushedAt": PUSHED_AT,
    "runLink": RUN_LINK
  },
  "relations": {}
}

## Finally ingest the entity data to the blueprint
response = requests.post(f'{API_URL}/blueprints/{blueprint_id}/entities?upsert=true', json=entity, headers=headers)
print(response.json())