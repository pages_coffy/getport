# Port Integration with Gitlab and Sentry


## Getting started

This document describes my approach to creating 2 integrations with Gitlab and Sentry. 

### Task 1: Ingesting Data to Port using GitLab CI
[Go to this link ](./assignment_1.md)

### Task 2: Creating a Webhook for Sentry.io 
[Go to this link ](./assignment_2.md)