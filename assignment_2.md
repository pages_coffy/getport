# Streaming Sentry Events to Port Using Webhooks

##### Article by: Isaac Coffie
##### Organization ID: assignment-isaac

![banner image](./assets/port_vs_sentry.png "Main banner image")

In this tutorial, we will learn how to stream events from Sentry to Port using webhooks. Specifically, we shall learn to ingest real-time comments data from Sentry to Port. This will help us track all comments about errors or issue alerts and build our software catalogue.

Webhooks are a simple and effective way to communicate data between two applications in real time. When an event occurs in Sentry.io, a payload is sent to the webhook URL defined in Port. This payload contains information about the event, such as the issue ID, the message, and other relevant data.

To create a webhook integration between Port and Sentry.io, follow these steps:

## Step 1: Create your Blueprint
Blueprints are the building blocks of the software catalogue in Port. They allow you to define the asset you want to catalogue. In our case, we will create a blueprint for the comments events on Sentry.io. When you visit Sentry’s documentation, you will realise that a comment object has the following schema:

```
data["comment"]
data["project slug"]
data["comment_id"]
data["issue_id"]
data["timestamp"]

```

So, copy and paste the following blueprint JSON file into your Port dashboard.

```json
{
    "identifier": "SentryComments",
    "description": "This blueprint represents a Sentry comment webhook in our software catalog",
    "title": "Sentry Comments Issues",
    "icon": "Deployment",
    "schema": {
      "properties": {
        "action": {
          "type": "string",
          "title": "action"
        },
        "comment": {
          "type": "string",
          "title": "comment"
        },
        "project": {
          "type": "string",
          "title": "project slug"
        },
        "comment_id": {
          "type": "string",
          "title": "comment id"
        },
        "issue_id": {
          "type": "string",
          "title": "ID of Issue"
        },
        "timestamp": {
          "type": "string",
          "title": "Timestamp of comment"
        }
      },
      "required": []
    },
    "mirrorProperties": {},
    "calculationProperties": {},
    "relations": {}
  }

```

## Step 2: Create Webhook Configuration Metadata
The metadata configuration of the webhook includes all properties related to how your events and data will be displayed inside Port’s UI. Copy and paste the following metadata JSON file into your code environment. We shall use it in step 3. We must consider security and ensure the webhook requests come from Sentry.

```json
{
  "identifier": "SentryMapper",
  "title": "Sentry mapper",
  "description": "A webhook configuration to map Sentry Comments to Port",
  "icon": "Deployment",
  "mappings": [
    {
      "blueprint": "SentryComments",
      "entity": {
        "identifier": ".body.data.comment_id",
        "title": "Comment Event",
        "properties": {
          "action": ".body.action",
          "comment": ".body.data.comment",
          "project": ".body.data.project_slug",
          "comment_id": ".body.data.comment_id",
          "issue_id": ".body.data.issue_id",
          "timestamp": ".body.data.timestamp"
        }
      }
    }
  ],
  "enabled": true,
  "security": {
    
  }
}

```

## Step 3: Getting Webhook URL
The next step is to create the webhook URL that will be used to stream data from Sentry to Port. Open a POSTMAN or any REST client to request the Port API. Below is information about this API call.

METHOD: POST

Authentication: Bearer JWT TOKEN (which you can generate from your Port UI)

Body: Copy and paste the webhook metadata from step 2

Once successful, you will receive your webhook URL in the response body of this API call. It typically follows this format as shown below: https://ingest.getport.io/{webhookKey}

![POSTMAN REST image](./assets/postman_api.PNG "Making POSTMAN request")


## Step 4: Connecting the Webhook URL with Sentry
The final step is connecting the webhook URL obtained from step 3 into Sentry. Log in to sentry.io and go to “Settings” > “Developer Settings” > “Create Integration”.

You will be presented with a form to fill in the details of your webhook integration. There are 3 key things to note on this form.

The webhook URL: Copy and paste the Port webhook URL
Events: You can select the type of events you want to stream. Make sure to select comment since that’s the focus of this tutorial.
Permission: Make sure to grant this integration the appropriate read/write permission.
A screenshot of this process is shown below.

![Sentry Webhook image](./assets/port_sentry_webhook.PNG "Sentry Webhook")

## Step 5: Trigger Webhook Events
To confirm that your webhook integration was successful, navigate to one of your issues or alerts in Sentry and comment on it. Here is a screenshot showing the comments happening in Sentry.

Let’s now turn to our Port UI and see the data ingested into our Sentry Comments blueprint.
![Sentry Comments image](./assets/sentry_comments_port.PNG "Sentry Comments")


## Step 6: Configuring Secrets
Since we have the integration working correctly, let's add another layer of security to it -by configuring secrets. This is one way of verifying the integrity of the incoming data. After we created the Sentry integration, we were presented with a client secret.
Now copy that secret key and update the content of your Webhook configuration in step 3. If you read the documentation on Sentry, you will also realize that the signature Algorithm is SHA256, and it passed to the request header as "sentry-hook-signature". The documentation also does not mention anything on the signature prefix. Because of that, we will leave that part blank.

METHOD: PATCH
Authentication: Bearer JWT TOKEN (which you can generate from your Port UI)
Body: 
```
  "security": {
    "secret": "YOUR_SENTRY_SECRET_TOKEN",
    "signatureHeaderName": "sentry-hook-signature",
    "signatureAlgorithm": "sha256",
    "signaturePrefix": ""
  }

```
Try making a comment again in Sentry and watch it stream to Port in real time.

And that’s it. We’ve finally created a webhook integration to stream live comments from Sentry to Port.