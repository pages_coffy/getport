import sentry_sdk
from decouple import config

sentry_sdk.init(
    dsn= config("SENTRY_DSN"),
    traces_sample_rate=1.0
)
division_by_zero = 1 / 0
