# Ingesting Data from Gitlab CI Pipeline to Port

##### Article by: Isaac Coffie
##### Organization ID: assignment-isaac

![banner image](./assets/port_vs_gitlab.png "Main banner image")

This tutorial will use Python and GitLab CI pipeline to ingest data to [Port](https://app.getport.io/)

## Overview
Port is a platform that gives developers a holistic understanding of their development lifecycle and underlying architecture. The platform also reduces the cognitive load from complex architectures, tool proliferation and tribal knowledge.

The following step shall enable us to ingest/stream data from Gitlab CI to Port.

### Step 1: Create a Port CLIENT and SECRET keys
The first thing you need to do is create a Port API key. You can do this by signing up and logging into your Port account. After a successful login, navigate to the “Help” tab. From here, click “Credentials” and follow the prompts to generate your client and secret keys.

Once you have your API keys, keep them safe and secure. You will need it in the following steps.

### Step 2: Create your Blueprint
To build your software catalogue — one of the primary use cases of the Port platform–we need to define blueprint schemas. Blueprints represent assets that can be managed in Port, such as Microservice, Environments, Packages, Clusters, Databases, and many more.

In our example, we will define a blueprint representing Gitlab commit data. Use the following code snippet to create your Blueprint in the Port app.

```json
{
  "identifier": "gitlab_commit",
  "title": "Gitlab History",
  "icon": "Git",
  "schema": {
    "properties": {
      "version": {
        "type": "string",
        "title": "Version"
      },
      "committedBy": {
        "type": "string",
        "title": "Committed By"
      },
      "commitHash": {
        "type": "string",
        "title": "Commit Hash"
      },
      "actionJob": {
        "type": "string",
        "title": "Action Job"
      },
      "repoPushedAt": {
        "type": "string",
        "format": "date-time",
        "title": "Repository Pushed At"
      },
      "runLink": {
        "type": "string",
        "format": "url",
        "title": "Action Run Link"
      }
    },
    "required": []
  },
  "relations": {},
  "calculationProperties": {}
}
```
Once successful, you should see something similar to the image below on your Port dashboard.

![blueprint image](./assets/blueprint_schema.PNG "Blueprint Creation")

With our blueprint created successfully, we can head to Gitlab to create a project and add entries.

### Step 3: Create a GitLab repository
You need to create a new GitLab repository. This is where you will store your Python script and any other files to ingest data to Port.

To create a new GitLab repository, log into your GitLab account and click on the “New project/repository” button. From here, follow the prompts to create a new repository.

### Step 4: Ingest Data using the Port API
Port’s beautiful REST API enables developers to seamlessly ingest data to their blueprints and catalogue. We must install a few libraries using the python-pip command to achieve this. We will use requests and decouple for making HTTPS requests and accessing environment variables, respectively.

```
pip install requests
pip install python-decouple
```

Now, create a python file and name it main.py

Copy and paste the below content into it.


```python
## Import the needed libraries
import requests
import uuid
from decouple import config


# Env vars passed by the config object
CLIENT_ID = config('CLIENT_ID')
CLIENT_SECRET = config('CLIENT_SECRET')
API_URL = 'https://api.getport.io/v1'


## Try getting the Access Token from the Port API
credentials = {'clientId': CLIENT_ID, 'clientSecret': CLIENT_SECRET}
try:
    token_response = requests.post(f'{API_URL}/auth/access_token', json=credentials)
    token_response.raise_for_status() # This will raise an exception for HTTP status codes >= 400
    access_token = token_response.json()['accessToken']
    headers = {'Authorization': f'Bearer {access_token}'} # We can now use the value in access_token to make further requests


# Handle any HTTP error that may occur
except requests.exceptions.HTTPError as err:
    print(f'HTTP error occurred: {err}') 
    raise SystemExit(err) # Stop further execution in case of error

# Handle any other error that may occur
except Exception as err:
    print(f'Other error occurred: {err}')
    raise SystemExit(err) # Stop further execution in case of error


# We can now use the value in access_token to make further requests
headers = {
	'Authorization': f'Bearer {access_token}'
}

## Reference the blueprint
blueprint_id = 'gitlab_commit'

# Get entity variables
COMMIT_BY = config('COMMIT_BY')
COMMIT_HASH = config('COMMIT_HASH')
ACTION_JOB= config('ACTION_JOB')
PUSHED_AT = config('PUSHED_AT')
RUN_LINK = config('RUN_LINK')


## create a unique ID
identifier_id = uuid.uuid4().hex

## Prepare the entity object
entity = {
  "identifier": identifier_id,
  "title": "Stream Gitlab Commits",
  "properties": {
    "version": "0.01",
    "committedBy": COMMIT_BY,
    "commitHash": COMMIT_HASH,
    "actionJob": ACTION_JOB,
    "repoPushedAt": PUSHED_AT,
    "runLink": RUN_LINK
  },
  "relations": {}
}

## Finally ingest the entity data to the blueprint
response = requests.post(f'{API_URL}/blueprints/{blueprint_id}/entities?upsert=true', json=entity, headers=headers)
print(response.json())

```

### Step 5: Setting up the Gitlab CI
The final step is to set up GitLab CI to automatically run your Python script whenever you push changes to your repository.

The first thing we need to do is to configure Gitlab to access our Port API keys. To do so, head to your Gitlab repository and navigate to Settings > CI/CD > Variables. Add your client and secret key, as seen below.


![Gitlab CI image](./assets/env_variables.PNG "Gitlab CI variables")

Next, create a new file in your Python project called “.gitlab-ci.yml” and paste in the following code:

```
image: python:3.8

variables:
  CLIENT_ID: $CLIENT_ID
  CLIENT_ID: $CLIENT_SECRET
  COMMIT_BY: $CI_COMMIT_AUTHOR
  COMMIT_HASH: $CI_COMMIT_SHA
  ACTION_JOB: $CI_JOB_NAME
  PUSHED_AT: $CI_COMMIT_TIMESTAMP
  RUN_LINK: $CI_PIPELINE_URL

stages:
  - build

ingest_data_to_port:
  stage: build
  before_script:
    - python --version
    - python -m pip install --upgrade pip
    - pip install --upgrade setuptools
    - pip install --no-cache-dir -r requirements.txt
  script:
    - python main.py
```

This code tells GitLab to run your Python script using the “python:3.8” image. Whenever you push changes to your repository, GitLab will automatically run this script.

You can view the content of the ingested data on Port. See the image below.

![Port UI image](./assets/gitlab_history_in_port.PNG "Port UI image")

And that’s it! You now have a fully functional GitLab CI pipeline that ingests data to Port using Python.